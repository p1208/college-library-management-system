#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Library.h"
#include "Date.h"

// #FOr_Common_functions

// Output #payment_display
void payment_display(payment_t *p) {
	printf("Payment ID : %d\tMember ID : %d\tMemeber Type : %s\tAmount: %.2lf\n", p->id, p->memberid, p->type, p->amount);

	printf("Payment : ");
	// Logic #date_printing
	date_print(&p->tx_time);

	// Logic #due_date_printing 
	printf("Payment due : ");
	date_print(&p->next_pay_duedate);
}

//Input #payment_acept
void payment_accept(payment_t *p) {

	
	p->id = get_next_payment_id();

	printf("member id: ");
	scanf("%d", &p->memberid);

	printf("type (fees/fine): ");
	scanf("%s", p->type);
	
	strcpy(p->type,PAY_TYPE_FEES);

	printf("amount: ");
	scanf("%lf", &p->amount);

	p->tx_time = date_current();
	if(strcmp(p->type, PAY_TYPE_FEES) == 0)
	// because_of_hardcore_payment_type
		p->next_pay_duedate = date_add(p->tx_time, MEMBERSHIP_MONTH_DAYS);
	else
		memset(&p->next_pay_duedate, 0, sizeof(date_t));
	
	// Output #payment_display
	date_print(&p->next_pay_duedate);
}

// Logic #autogenrated_payment_id
int get_next_payment_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(payment_t);
	payment_t u;

	// Loogic #open_the_file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
		return max + 1;

	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);

	// Logic #read_record_from_db
	if(fread(&u, size, 1, fp) > 0)
		// Logic #mx_id
		max = u.id;
	fclose(fp);
	
	return max + 1;
}

// ______________________________________________________

// Output #issued_book_of_member
void display_issued_bookcopies(int member_id) {
	FILE *fp;
	issuerecord_t rec;
	
	// Logic #open_issue_records_db
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp==NULL) {
		perror("Can not open issue record file");
		return;
	}

	// Logic #read_records_one_by_one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// Output #display
		if(rec.memberid == member_id && rec.return_date.day == 0)
			issuerecord_display(&rec);
	}
	fclose(fp);
}

// Output #issue_record_of_books
void issuerecord_display(issuerecord_t *r) {
	printf("issue record: %d\tcopy: %d\tmember: %d\tfine: %.2lf\t", r->id, r->copyid, r->memberid, r->fine_amount);
	printf("issue ");
	date_print(&r->issue_date);
	printf("\treturn due ");
	date_print(&r->return_duedate);
	printf("\treturn ");
	date_print(&r->return_date);
}

// Input #Issue_record_of_books
void issuerecord_accept(issuerecord_t *r) {

	//printf("id: ");
	//scanf("%d", &r->id);

	r->id = get_next_issuerecord_id();

	printf("Copy id: ");
	scanf("%d", &r->copyid);

	printf("Member id: ");
	scanf("%d", &r->memberid);

	// Logic #entering_backdated_entries
	printf("Issue Date");
	date_accept(&r->issue_date);

	// Logic #taking_current_system_date
	//r->issue_date = date_current();

	// Logic #expected_return_date
	r->return_duedate = date_add(r->issue_date, BOOK_RETURN_DAYS);

	// Logic #itialize_zero by memset
	// memset(x,0,y) means from x, y bites are zero
	memset(&r->return_date, 0, sizeof(date_t));

	r->fine_amount = 0.0;
}

// Logic #autogenrate_next_issuerecord_id
int get_next_issuerecord_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t u;

	// Logic #open_db
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;

	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);
	// Logic #read_record_from_db
	if(fread(&u, size, 1, fp) > 0)
		// Logic #max_id
		max = u.id;
	fclose(fp);
	
	return max + 1;
}

//________________________________________________________

 // Output #book_info
void bookcopy_display(bookcopy_t *c) {
    printf("Copy ID : %d, Book ID : %d, Rack No. : %d, Status : %s\n", c->id, c->bookid, c->rack, c->status);
}

// Input #book_copy_details
void bookcopy_accept(bookcopy_t *c) {

	// Logic #autogenrated_bookcopy_id
	c->id = get_next_bookcopy_id();

	printf("Book ID : ");
	scanf("%d", &c->bookid);

	printf("Rack No. : ");
	scanf("%d", &c->rack);

	// Logic #acessing_only
	strcpy(c->status, STATUS_AVAIL);
}

// Logic #genrating_next_user_id
int get_next_bookcopy_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(bookcopy_t);
	bookcopy_t u;

	// Logic #opening_db
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);
	// Logic #read_record_from_db
	if(fread(&u, size, 1, fp) > 0)
		// Lgic #mx_id
		max = u.id;
	fclose(fp);
	
	return max + 1;
}

//___________________________________________________________

// Logic #find_book_by_partial_name
void book_find_by_name(char name[]) {
	FILE *fp;
	int found = 0;
	book_t b;

	// Logic #open_file
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL) {
		perror("Failed to open books file.\n");
		return;
	}

	// Logic #read_record_from_db_one_by_one
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		// Logic #matching_name_partially
		if(strstr(b.name, name) != NULL) {
			found = 1;
			book_display(&b);
		}
	}
	
	fclose(fp);
	// Logic #nothing_found
	if(!found)
		printf("No such book found.\n");
}

// Logic #genrating_next_Book_id
int get_next_book_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(book_t);
	book_t u;

	// Logic #open_file
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);

	// Logic #read_record_from_db
	if(fread(&u, size, 1, fp) > 0)
		// Lgoc #mx_id
		max = u.id;
	
	fclose(fp);

	return max + 1;
}

// Input #book_info
 void book_accept(book_t *b) {

	b->id = get_next_book_id();
     
	printf("Name: ");
	scanf("%s", b->name);

	printf("Author: ");
	scanf("%s", b->author);

	printf("Subject: ");
	scanf("%s", b->subject);

	printf("Price: ");
	scanf("%lf", &b->price);

	printf("ISBN: ");
	scanf("%s", b->isbn);
 }

 // Output #book_info
 void book_display(book_t *b) {
     printf("Book ID : %d, Name : %s, Author : %s, Subject : %s, Price : %.2lf, ISBN No. : %s\n", b->id, b->name, b->author, b->subject, b->price, b->isbn);
 }

// _________________________________________________________

// Logic #Generic_change_password
void change_password(user_t *u){
	FILE *fp;

	// Logic #open_db
	user_t m;
	char currPass[20],conPass[20];
	// open users file.
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("Can not open users file\n");
		exit(1);
	}
	// read users one by one and check if member with given id is found.
	while(fread(&m, sizeof(user_t), 1, fp) > 0) {
		if(m.id == u->id) {
			printf("Current Password : ");
			scanf("%s",currPass);
			if(strcmp(currPass,m.password) == 0){
				printf("NEW PASSWORD : ");
				scanf("%s",m.password);
				printf("CONFIRM PASSWORD : ");
				scanf("%s",conPass);
				if(strcmp(m.password, conPass) == 0){
					long size = sizeof(user_t);
					// File postition one record behind
					fseek(fp, -size , SEEK_CUR);
					user_display(&m);
					//Overwrite details in file
					fwrite(&m, size ,1, fp);
					printf("Password Updated Successfully....!\n");
				}else
					printf("Password didn't match....!\n");	
				break;
			}else
					printf("Password didn't match....!\n");	
				break;
		}
		}
		fclose(fp);
}

// Logic #edit_profile
void edit_profile(user_t *u){
	FILE *fp;
	char mail[50],name[50],phone[15];
	int found=0;
	user_t newu;

	// Logic #open_db
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("Can not open users file.\n");
		exit(1);
	}

	//Logic #input new Member from user
	long size = sizeof(user_t);
	
	// Logic #find_user_id
		while(fread(&newu, sizeof(user_t), 1, fp) > 0) {
			if(newu.id == u->id ) {
				user_display(u);
				//user_accept(&newu);
				newu.id = u->id;

				printf("Name: ");
	            		scanf("%s", newu.u_name);

	 			printf("Email: ");
	 			scanf("%s", newu.email);

				printf("Phone: ");
				scanf("%s", newu.phone);

				//printf("password: ");
				//scanf("%s", u->password);

				strcpy(newu.password,u->password);
				
				strcpy(newu.role, u->role );
							
				// Logic #File_pointer_one_record_behind
				fseek(fp, -size , SEEK_CUR);

				// Output #before
				printf("BEFORE : \n");
				user_display(u);

				// Logic #OverWrite_Book_Details
				fwrite(&newu, size ,1, fp);

				// Ouput #after
				printf("AFTER : \n");
				user_display(&newu);
				
				printf("Profile Updated Successfully.....!\n");
				break;

			}
		}
	fclose(fp);	
}

// Logic #genrating_next_user_id
int get_next_user_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(user_t);
	user_t u;

	// Logic #open_file
	fp = fopen(USER_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);

	// Logic #read_record_from_db
	if(fread(&u, size, 1, fp) > 0)
		// Lgoc #mx_id
		max = u.id;
	
	fclose(fp);

	return max + 1;
}

// Logic #finding_user_by_email
int user_find_by_email(user_t *u, char email[]) {
   // #Declaration
	FILE *fp;
	int found = 0;

	// #open_the_file_for_reading_the_data
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) {
		perror("Failed to open users file.\n");
		return found;
	}

	// #read_all_users_one_by_one
	while(fread(u, sizeof(user_t), 1, fp) > 0) {
		// #if_user_email_is_matching_found_1
		if(strcmp(u->email, email) == 0) {
			found = 1;
			break;
		}
	}
	// #close_file
	fclose(fp);
	// #return
	return found;
}

 // Logic #adiing_user_to_db
 void user_add(user_t *u) {
	// #open_the_db_for_appending_the_data
	FILE *fp;
	fp = fopen(USER_DB, "ab");
	if(fp == NULL) {
		perror("Failed to open users file.\n");
		return;
	}
	
	// #write_user_data_into_the_db
	fwrite(u, sizeof(user_t), 1, fp);
	printf("User added into file.\n");
	
	// #close_the_db
	fclose(fp);
}

// Input #user_info
void user_accept(user_t *u){
    //printf("ID : ");
    //scanf("%d", &u->id);

    u->id = get_next_user_id();

    printf("Name : ");
    scanf("%s", u->u_name);

    printf("Email : ");
    scanf("%s", u->email);

    printf("Phone : ");
    scanf("%s", u->phone);

    printf("Password : ");
    scanf("%s", u->password);

    // Logic #acessing_only
    strcpy(u->role,ROLE_STUDENTS);
}

// Output #User_info
void user_display(user_t *u){
    printf("ID : %d, Name : %s, Email : %s, Phone : %s, Role : %s\n",u->id,u->u_name,u->email,u->phone,u->role);
}