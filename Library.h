#ifndef _LIBRARY_H
#define _LIBRARY_H

#include "Date.h"

 // Defineing #owner info
 #define EMAIL_S_LIBRARIAN "emer@aislinninfo.com"

// Declarations #db_details
#define USER_DB "users.db"
#define BOOK_DB "books.db"
#define BOOKCOPY_DB "bookcopies.db"
#define ISSUERECORD_DB	"issuerecord.db"
#define PAYMENT_DB	"payment.db"

// #FOr_structures_and_functions_declearation
#define ROLE_S_LIBRARIAN "libraryian_s"
#define ROLE_A_LIBRARIAN "libraryian_a"
#define ROLE_STUDENTS    "student"

// Declaration #book_status
 #define STATUS_AVAIL "available"
 #define STATUS_ISSUED "issued"

// Declaration #due_dates
 #define BOOK_RETURN_DAYS      7
 #define FINE_PER_DAY          5
 #define MEMBERSHIP_MONTH_DAYS 30

// Declaration #payment_type
 #define PAY_TYPE_FEES "fees"
 #define PAY_TYPE_FINE "fine"

// #Logic : To_store_user_data
typedef struct user{
    int id;
    char u_name[30];
    char email[30];
    char phone[15];
    char password[10];
    char role[15]; // Librarian_s Librarian_a students
}user_t;

// #Logic : To_store_book_data
typedef struct book {
    int id;
	char name[50];
	char author[50];
	char subject[30];
	double price;
	char isbn[16];
}book_t;

// #Logic : To_store_book_Copies_data
typedef struct bookcopy {
    int id;
	int bookid;
	int rack;
	char status[16];
 }bookcopy_t;

// #Logic : To_store_issuerecord_data
 typedef struct issuerecord {
     int id;
	 int copyid;
	 int memberid;
	 date_t issue_date;
	 date_t return_duedate;
	 date_t return_date;
	 double fine_amount;
 }issuerecord_t;

// #Logic : To_store_user_data
typedef struct payment {
     int id;
	 int memberid;
	 double amount;
	 char type[10];
	 date_t tx_time;
	 date_t next_pay_duedate;
 }payment_t;

// #common_level_functions
void user_accept(user_t *u);
void user_display(user_t *u);
void user_add(user_t *u);

void sign_up();

int user_find_by_email(user_t *u, char email[]);
void sign_in();

int get_next_user_id();

void edit_profile(user_t *u);
void change_password(user_t *u);

void display_issued_bookcopies(int member_id);

int get_next_payment_id();
void payment_accept(payment_t *p);
void payment_display(payment_t *p);

// #senior_librarian_level_functions
void s_librarian_area(user_t *u);
void appoint_a_librarian();

// #Assistant_librarian_level_fuctions
void a_librarian_area(user_t *u);
void add_student();

void change_rack();
void view_all_users();
void view_all_books();
void view_all_book_copies();
void view_all_issue_record();

void payment_history(int memberid);
int is_paid_member(int memberid);

void fine_payment_add(int memberid, double fine_amount);

 // #book_functions
 int get_next_book_id();
 void book_accept(book_t *b);
 void book_find_by_name(char name[]);
 void book_display(book_t *b);

  void book_edit_id();

  int get_next_bookcopy_id();
  void bookcopy_accept(bookcopy_t *c);
  void bookcopy_add();
  void bookcopy_display(bookcopy_t *c);
  void bookcopy_checkavail_details();

  // #issuerecord_functions
  int get_next_issuerecord_id();
  void bookcopy_changestatus(int bookcopy_id, char status[]);
  void issuerecord_accept(issuerecord_t *r);

  void issuerecord_display(issuerecord_t *r);
  void bookcopy_return();

 // #payment_functions
 void payment_add();

// #Student_level_functions
void student_area(user_t *u);
void bookcopy_checkavail();

#endif