#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Library.h"
#include "Date.h"

// #FOr_Senior_Librarian_functions

// Logic #appoint_librarian
void appoint_a_librarian() {
	// input #librarian_details
	user_t u;
	user_accept(&u);

	// Logic #labriran_role
	strcpy(u.role, ROLE_A_LIBRARIAN);

	// Logic #add_librarian_into_db
	user_add(&u);
}


// Output #owner_menu
void s_librarian_area(user_t *u) {
    // #Declearation
    int choice;

    // Logic #member_menu
	do {
                    printf("************** OWNER : WELCOME %s **************",u->u_name);
		printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\nEnter choice : ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // #appoint_a_labrarian
                                        appoint_a_librarian();
				break;
			case 2:// #Edit_Profile
				edit_profile(u);
				break;
			case 3:// #Change_Password
				change_password(u);
				break;
		}
	}while (choice != 0);
}
