# College Library Management System

# For making executable file

    1. Compile 
    
        a. => .exe  // For Windows Based Paltform
        gcc -o Library.exe Main.c Librarian_s.c Librarian_a.c Students.c Common.c Date.c

        b. => .out // For Linux Based Paltform
        gcc -o Library.out Main.c Librarian_s.c Librarian_a.c Students.c Common.c Date.c

    2. .exe / .out => execute to see display
        ./Library.exe / ./Library.out

# Role
    1. Senior Librarian
    2. Assistant Librarian
    3. Student

# Day 01
    1. Basic skaleton code
    2. User's structures and their I/O

# Day 02
    1. Main menu and sign up functionality
    2. Skaleton menu of senior librarian, assistant librarian  & students
    3. Sign in functionility of main menu & owner role is to be given
    4. Appoint assistant librarian functionility of senior librarian

# Day 03

    1. Auto genrated user ID
    2. Student add functionality for assistant Librarian
    3. Edit profile
    4. change password

# Day 04

    1. Book add Functionality of assistant librarian
    2. Find book Functionality of assistant librarian and students

# Day 05

    1. Book edit functionality of Assistant Librarian
    2. Book Copy add of Assistant Librarian
    3. Book Avalibilities Details of Assistant Librarian

# Day 06

    1. Essential date functions are added 
    2. Issue Book Functionality of Assistant Librarian are completed

# Day 07

    1. Book Avalibilities For Stuents
    2. Book Copies return logic completed
    3. Change rack Functionality of  Assistant Librarion
    4. view all users
    5. view all books 
    6. view all book copies 
    7. view all issued record

# Day 08

    1. Payment Taken Functionality of assistant Librarian
    2. Payment history display functionality of assistant librarian
    3. Checking whether member is paid or not
    4. ToDo1: while issue copy, Checking whether member is paid or not
    5. ToDo2: fine on delayed book while returen
