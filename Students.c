#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Library.h"
#include "Date.h"

// #FOr_Student_functions

// Logic #check_book_avalibilty
void bookcopy_checkavail() {
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;

	// Logic #input_book_id
	printf("Enter the book ID : ");
	scanf("%d", &book_id);
	// Logic open_bookcopies_db
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("Cannot open bookcopies file.\n");
		return;
	}

	// Logic #read_bookcopy_records_one_by_one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// Logic #afetr_checking_avalibility_print_bookcopy_details
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL)==0) {
		//	bookcopy_display(&bc);
			count++;
		}
	}
	fclose(fp);
	// Output #avalibility_count
	printf("Number of copies availables: %d\n", count);
}

// Output #member_menu
void student_area(user_t *u) {
	// #Declaration
	int choice;
	char name[80];

	// Logic #member_menu
	do {
		printf("************** MEMBER : WELCOME %s **************",u->u_name);
		printf("\n\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. Change Password\n4. Book Availability\n5. Issued Book\nEnter choice : ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // #find_book
				printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);
				break;
			case 2:// #Edit_Profile
				edit_profile(u);
				break;
			case 3:// #Change_Password
				change_password(u);
				break;
			case 4: // #Book_Availability
				bookcopy_checkavail();
				break;
			case 5 : // #List_Issued_Book
				display_issued_bookcopies(u->id);
				break;
		}
	}while (choice != 0);	
}