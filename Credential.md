
# user.db 

## Senior Librarian's Login

 1. 
    Name : emer
    Email : emer@aislinninfo.com
    Phone : 12345
    Password : emer123

## Assistant Librarin's Login

 2. 
    Name : vishal
    Email : vishal@emerinfo.com
    Phone : 23478
    Password : vishal

  3. 
    Name : riya
    Email : riya@emerinfo.com
    Phone : 34567
    Password : riya234

## Student's Login

  4. 
     Name : Angha
     Email : Angha@emerinfo.com
     Phone : 2356
     Password : angha654

  5. 
     Name : Varun
     Email : varun@emerinfo.com
     Phone : 2378
     Password : Varun56

  6. 
     Name : Ram
     Email : Ram@emerinfo.com
     Phone : 29534
     Password : Ram05

# books.db 

 1. 
    name: C_Programming_Language
    author: Ritche
    subject: C
    price: 213.7
    isbn: 7539543

 2. 
    name: C++_Programming_Language
    author: Stroustrup
    subject: CPP
    price: 310.2
    isbn: 2345

 3. 
    name: JAVA_Programming_Language
    author: Ram
    subject: JAVA
    price: 537  
    isbn: 23456

 4. 
    name: Yugandhar
    author: Shivaji_Sawant
    subject: Novel
    price: 1000.4
    isbn: 23456

# bookcopies.db 

 1. 
    Book ID : 1
    Rack No. : 3

 2. 
    Book ID : 1
    Rack No. : 5

 3. 
    Book ID : 2
    Rack No. : 3

 4. 
    Book ID : 2
    Rack No. : 3

 5. 
    Book ID : 2
    Rack No. : 3

# issuerecord.db

1. 
   Copy ID : 4
   Membe ID : 4
   Issue due date : 15 9 2021

 2. 
    Copy id: 2
    Member id: 4
    Issue Datedate: 10 9 2021