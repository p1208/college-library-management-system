#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Library.h"
#include "Date.h"

// #For_primary_function

void date_tester() {
    date_t d1 = {1, 1, 2000}, d2 = {31, 12, 2000};
    date_t d = {1, 1, 2000};
    date_t r = date_add(d, 366);
    date_print(&r);
    int diff = date_cmp(d1, d2);
    printf("Date diff: %d\n", diff);
}

// #Logic : For testing User I/O
void u_tester(){
    user_t u;
    user_accept(&u);
    user_display(&u);
}

// #Logic : For testing sign_up functionality Functionality
void sign_up() {

    // Testing #adding_user_to_db
	 // Input #user_details_from_the_user.
       user_t u;
       user_accept(&u);

	 // #Logic : write user details into the users file.
       user_add(&u);
}

// #Logic : For Sign in the user
void sign_in() {
    // #Declaration
    char email[30], password[10];
	user_t u;
	int invalid_user = 0;

	// input #email and password
	printf("email: ");
	scanf("%s", email);
	printf("password: ");
	scanf("%s", password);

	// Logic #finding_user_in_db_by_email
	if(user_find_by_email(&u, email) == 1) {
		// Logic #password_verification
		if(strcmp(password, u.password) == 0) {
			// Logic #owner_or_not
			if(strcmp(email, EMAIL_S_LIBRARIAN) == 0)
				strcpy(u.role, ROLE_S_LIBRARIAN);

			// Logic #call_user_area()_based_on_role.
			if(strcmp(u.role, ROLE_S_LIBRARIAN) == 0)
				s_librarian_area(&u);
			else if(strcmp(u.role, ROLE_A_LIBRARIAN) == 0)
				a_librarian_area(&u);
			else if(strcmp(u.role, ROLE_STUDENTS) == 0)
				student_area(&u);
			else
				invalid_user = 1;
		}
		else
			invalid_user = 1;
	}
	else
		invalid_user = 1;

	if(invalid_user)
		printf("Invalid email, password or role.\n");
}

int main(void){

    //#Skaleton_testing
    //printf("Good Morning\n");

    //#testing_user
    //u_tester();

    // Declaration
    int choice;
    
    // testing #next_id_function
    //printf("new id :%d\n",get_next_user_id());

    // testing #date_functions
    //date_tester();

    // testing #is_member_paid
    //printf("Is paid %d\n",is_paid_member(4));

    // Output #main_menu
	do {
		printf("\n\n0. Exit\n1. Sign In\n2. Sign Up\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: // Sign In
            		sign_in();
			break;
		case 2:	// Sign Up
			sign_up();
			break;
		}
	}while(choice != 0);

    return 0;
}